package com.softlutiontech.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmpleadoServiceDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmpleadoServiceDemoApplication.class, args);
	}

}
