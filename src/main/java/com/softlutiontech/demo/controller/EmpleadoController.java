package com.softlutiontech.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.softlutiontech.demo.dto.EmpleadoReq;
import com.softlutiontech.demo.dto.EmpleadoResp;
import com.softlutiontech.demo.service.EmpleadoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

@RestController
@RequestMapping("/")
@Api(value = "/empleado", tags = "CRUD para tabla empleado")
public class EmpleadoController {
	private static final Logger log = LoggerFactory.getLogger(EmpleadoController.class);
	
	@Autowired
	private EmpleadoService service;
	
	@ApiOperation(value = "Ejecuta el proceso para guardar un empleado", response = EmpleadoResp.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"), @ApiResponse(code = 409, message = "Error")})
	
	@PostMapping(value = "/guardar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> guardarEmpleado(@RequestBody EmpleadoReq req, @RequestHeader HttpHeaders hReq) {
		return service.guardar(req);
	}

}
