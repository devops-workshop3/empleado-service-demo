package com.softlutiontech.demo.commons;

public class Constants {
	/**
	 * Instantiates a new constants.
	 */
	private Constants() {
		super();
	}
	
	
//	/** The Constant VALIDATE_HEADERS_MSG. */
	public static final String VALIDATE_HEADERS_MSG = "Headers validation result:";
	public static final String ACCESS_CONTROL = "access-control-expose-headers";
	public static final String ACCESS = "*";
	
	/** The Constant EXCEPTION_DESC. */
	public static final String EXCEPTION_DESC = "Unexpected Error";
	public static final String MAC_AUTHORIZATION = " mac_authorization";
	
	//LOGGER LEVEL
	public static final String CATEGORY_SERVICE = "service";
	public static final String CATEGORY_CONSUMER = "consumer";
	public static final String CATEGORY_TARGET = "target";
	public static final String UNKNOWNHOST = "unknownHost";
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	// MSGS ERROR
	public static final String MSG_ERROR = "ERROR";
	public static final String ERROR_PROCESS = "Ocurri� un error al procesar datos de entrada";
}
