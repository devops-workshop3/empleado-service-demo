package com.softlutiontech.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity(name = "empleado")
public class Empleado {
	@Id
	@Column(name = "emp_id")
	@SequenceGenerator(name = "emp_seq", allocationSize = 25)
	private int empId;
	@Column(name = "emp_name")
	private String empName;
	@Column(name = "emp_alias")
	private String empAlias;
	@Column(name = "emp_password")
	private String empPassword;
	@Column(name = "emp_dni")
	private String empDni;
	@Column(name = "emp_email")
	private String empEmail;

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpAlias() {
		return empAlias;
	}

	public void setEmpAlias(String empAlias) {
		this.empAlias = empAlias;
	}

	public String getEmpPassword() {
		return empPassword;
	}

	public void setEmpPassword(String empPassword) {
		this.empPassword = empPassword;
	}

	public String getEmpDni() {
		return empDni;
	}

	public void setEmpDni(String empDni) {
		this.empDni = empDni;
	}

	public String getEmpEmail() {
		return empEmail;
	}

	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}

}
