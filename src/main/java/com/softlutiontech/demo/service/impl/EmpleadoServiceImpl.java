package com.softlutiontech.demo.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.softlutiontech.demo.commons.Constants;
import com.softlutiontech.demo.dto.EmpleadoReq;
import com.softlutiontech.demo.dto.EmpleadoResp;
import com.softlutiontech.demo.model.Empleado;
import com.softlutiontech.demo.repository.EmpleadoRepo;
import com.softlutiontech.demo.service.EmpleadoService;
import com.softlutiontech.demo.util.AppUtil;

@Service
public class EmpleadoServiceImpl implements EmpleadoService {
	private static final Logger logger = LoggerFactory.getLogger(EmpleadoServiceImpl.class);

	private HttpStatus status;
	private HttpHeaders headers;

	@Autowired
	private EmpleadoRepo repository;

	@Autowired
	private AppUtil util;

	@Override
	public ResponseEntity<EmpleadoResp> guardar(EmpleadoReq req) {
		EmpleadoResp resp = new EmpleadoResp();
		headers = new HttpHeaders();

		try {
			Empleado emp = util.validateRequestToSave(req);
			logger.info("Se guard� el registro con �xito");
			resp = util.construirResp(repository.save(emp));
			status = HttpStatus.OK;
		} catch (Exception e) {
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			headers.add(Constants.MSG_ERROR, "Ocurri� un error al guardar el registro");
			logger.error("No se puede guardar empleado" + e.getMessage());
		}

		return new ResponseEntity<EmpleadoResp>(resp, headers, status);
	}

	@Override
	public EmpleadoResp getEmpleado(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<EmpleadoResp> getListaEmpleados() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EmpleadoResp actualizar(EmpleadoReq req) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EmpleadoResp eliminar(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
