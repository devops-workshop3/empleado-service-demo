package com.softlutiontech.demo.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.softlutiontech.demo.dto.EmpleadoReq;
import com.softlutiontech.demo.dto.EmpleadoResp;

public interface EmpleadoService {
	public ResponseEntity<EmpleadoResp> guardar(EmpleadoReq req);
	public EmpleadoResp getEmpleado(int id);
	public List<EmpleadoResp> getListaEmpleados();
	public EmpleadoResp actualizar(EmpleadoReq req);
	public EmpleadoResp eliminar(int id);
}
