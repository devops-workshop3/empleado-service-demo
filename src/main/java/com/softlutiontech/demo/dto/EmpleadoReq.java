package com.softlutiontech.demo.dto;

public class EmpleadoReq {
	private String empName;
	private String empAlias;
	private String empPassword;
	private String empDni;
	private String empEmail;

	public EmpleadoReq() {
		super();
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpAlias() {
		return empAlias;
	}

	public void setEmpAlias(String empAlias) {
		this.empAlias = empAlias;
	}

	public String getEmpPassword() {
		return empPassword;
	}

	public void setEmpPassword(String empPassword) {
		this.empPassword = empPassword;
	}

	public String getEmpDni() {
		return empDni;
	}

	public void setEmpDni(String empDni) {
		this.empDni = empDni;
	}

	public String getEmpEmail() {
		return empEmail;
	}

	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}

}
