package com.softlutiontech.demo.dto;

public class EmpleadoResp {
	private int empleadoId;
	private String nombreEmpleado;
	private String aliasEmpleado;
	private String correoEmpleado;

	public EmpleadoResp() {
		super();
	}

	public int getEmpleadoId() {
		return empleadoId;
	}

	public void setEmpleadoId(int empleadoId) {
		this.empleadoId = empleadoId;
	}

	public String getNombreEmpleado() {
		return nombreEmpleado;
	}

	public void setNombreEmpleado(String nombreEmpleado) {
		this.nombreEmpleado = nombreEmpleado;
	}

	public String getAliasEmpleado() {
		return aliasEmpleado;
	}

	public void setAliasEmpleado(String aliasEmpleado) {
		this.aliasEmpleado = aliasEmpleado;
	}

	public String getCorreoEmpleado() {
		return correoEmpleado;
	}

	public void setCorreoEmpleado(String correoEmpleado) {
		this.correoEmpleado = correoEmpleado;
	}

}
