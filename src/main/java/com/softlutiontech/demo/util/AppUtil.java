package com.softlutiontech.demo.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.softlutiontech.demo.dto.EmpleadoReq;
import com.softlutiontech.demo.dto.EmpleadoResp;
import com.softlutiontech.demo.model.Empleado;

@Component
public class AppUtil {
	private static final Logger log = LoggerFactory.getLogger(AppUtil.class);
	
	public Empleado validateRequestToSave(EmpleadoReq req) {
		Empleado emp = new Empleado();
		
		try {
			if (req != null) {
				emp.setEmpName(req.getEmpName());
				emp.setEmpAlias(req.getEmpAlias());
				emp.setEmpPassword(req.getEmpPassword());
				emp.setEmpDni(req.getEmpDni());
				emp.setEmpEmail(req.getEmpEmail());
			}
		} catch (Exception e) {
			throw e;
		}
		
		return emp;
	}
	
	public EmpleadoResp construirResp(Empleado req) {
		EmpleadoResp resp = new EmpleadoResp();
		
		try {
			if (req != null) {
				resp.setEmpleadoId(req.getEmpId());
				resp.setNombreEmpleado(req.getEmpName());
				resp.setAliasEmpleado(req.getEmpAlias());
				resp.setCorreoEmpleado(req.getEmpEmail());
			}
		} catch (Exception e) {
			throw e;
		}
		
		return resp;
	}
}
