package com.softlutiontech.demo.repository;

import org.springframework.stereotype.Repository;

import com.softlutiontech.demo.model.Empleado;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface EmpleadoRepo extends JpaRepository<Empleado, Integer> {

}
